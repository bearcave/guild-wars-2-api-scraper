namespace Guildwars2ApiScraper;
public static class IEnumerableExtensions
{
    public static IEnumerable<IEnumerable<TSource>> GetBuckets<TSource>(this IEnumerable<TSource> source, int bucketSize)
    {
        TSource[]? bucket = null;
        int count = 0;

        foreach (var item in source)
        {
            bucket ??= new TSource[bucketSize];

            bucket[count++] = item;
            if (count != bucketSize)
            {
                continue;
            }

            yield return bucket;
            bucket = null;
            count = 0;
        }

        if (bucket != null && count > 0)
        {
            yield return bucket.Take(count);
        }
    }
}
