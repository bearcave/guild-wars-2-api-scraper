
namespace Guildwars2ApiScraper;
public interface IApiScraper
{
    public Task ScrapeApiAsync(string endpoint, string filePrefix, string outputDirectory);
    public IAsyncEnumerable<IEnumerable<int>> GetIdsAsync(string endpoint);
}
