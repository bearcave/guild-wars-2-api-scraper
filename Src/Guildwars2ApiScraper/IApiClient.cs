
namespace Guildwars2ApiScraper;
public interface IApiClient{
    public Task<Stream> CallApiAsync(string endpoint);
    public string GetEndpointWithParams(string endpoint, IEnumerable<int> ids);
}
