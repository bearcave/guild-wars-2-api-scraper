namespace Guildwars2ApiScraper;

public class Guildwars2Api : IApiClient
{
    private readonly HttpClient client = new();

    public async Task<Stream> CallApiAsync(string endpoint)
    {
        var response =  await this.client.GetAsync(endpoint);
        return await response.Content.ReadAsStreamAsync();
    }

    public string GetEndpointWithParams(string endpoint, IEnumerable<int> ids) => endpoint + "?ids=" + string.Join(',', ids);
}
