using System.Text.Json;

namespace Guildwars2ApiScraper;

public class ApiScraper : IApiScraper
{
    private readonly IApiClient _apiClient;
    private readonly int _bucketSize = 200;
    public ApiScraper(IApiClient apiClient)
    {
        _apiClient = apiClient;
    }

    public async IAsyncEnumerable<IEnumerable<int>> GetIdsAsync(string endpoint)
    {
        using var responseStream = await _apiClient.CallApiAsync(endpoint);

        var doc = await JsonDocument.ParseAsync(responseStream);
        var buckets = doc.RootElement.EnumerateArray().Select(x => x.GetInt32()).GetBuckets(_bucketSize);
        
        foreach (var bucket in buckets)
        {
            yield return bucket;
        }
    }

    private string GetUniqueFileName(string prefix = "", string suffix = "", string extension = "out") 
        => $"{prefix}{Guid.NewGuid()}{suffix}.{extension}";

    public async Task ScrapeApiAsync(string endpoint, string filePrefix, string outputDirectory)
    {
        var buckets = GetIdsAsync(endpoint);
        var options = new ParallelOptions() { MaxDegreeOfParallelism = 4 };
        var prefix = $"{filePrefix}-";

        await Parallel.ForEachAsync(buckets,options, async(bucket,token) =>
        {
            var outputFileName = GetUniqueFileName(prefix:prefix,extension:"json");
            var outputPath = Path.Combine(outputDirectory,outputFileName);
            var endpointWithParams = _apiClient.GetEndpointWithParams(endpoint,bucket);
            
            using var responseStream = await _apiClient.CallApiAsync(endpointWithParams);
            using var outputStream = new FileStream(outputPath,FileMode.CreateNew,FileAccess.Write);

            await responseStream.CopyToAsync(outputStream);
            await outputStream.FlushAsync();
        });


    }
}
