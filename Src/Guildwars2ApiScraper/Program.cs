﻿// See https://aka.ms/new-console-template for more information
using Guildwars2ApiScraper;

var outputDirectory = @"C:\temp\guildwars2apiOutput";
var apiClient = new Guildwars2Api();
var scraper = new ApiScraper(apiClient);

await scraper.ScrapeApiAsync(@"https://api.guildwars2.com/v2/items", "items", outputDirectory);
await scraper.ScrapeApiAsync(@"https://api.guildwars2.com/v2/itemstats", "itemstats", outputDirectory);
await scraper.ScrapeApiAsync(@"https://api.guildwars2.com/v2/recipes", "recipes", outputDirectory);
await scraper.ScrapeApiAsync(@"https://api.guildwars2.com/v2/skins", "skins", outputDirectory);
await scraper.ScrapeApiAsync(@"https://api.guildwars2.com/v2/commerce/listings", "commerce-listings", outputDirectory);
